﻿using System;
using System.Collections.Generic;

namespace VisualBoxManager.Interfaces
{
    public interface ISavePdf
    {
        void Save(Move move, Box box);
    }
}