﻿using System;
using VisualBoxManager.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace VisualBoxManager
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Start : ContentPage
    {      
        public Start(IConnectionService connectionService)
        {
            InitializeComponent();
            BindingContext = new StartViewModel(connectionService);

        }
        /// <summary>
        /// Automatic logoff.
        /// If the user returns to start screen from MovePage,
        /// it will always make seens to call log out of the current session.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal void OnPopped(object sender, NavigationEventArgs e)
        {
            if (e.Page is MovePage)
                ((StartViewModel)BindingContext).LogOutAsync();
        }
    }
}
