﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VisualBoxManager.ConnectionServices;
using VisualBoxManager.views;
using Xamarin.Forms;

namespace VisualBoxManager.ViewModels.Home
{
    class ItemDetailViewModel : RootViewModel
    {
        private bool busy;
        private Item _item;
        private Move _move;
        private Box _box;
        private bool refreshing = false;

        public ItemDetailViewModel(IConnectionService connectionService, Move move, Box box, Item item) : base(connectionService)
        {
            _move = move;
            _box = box;
            _item = item;
            EditItemCommand  = new Command(() => EditItem(), () => !IsBusy);
            ChangeBoxCommand = new Command(() => ChangeBox(), () => !IsBusy);
            BackCommand      = new Command(() => { App.PopNavAsync(); }, () => !(IsBusy));
        }

        public Item item {
            get { return _item; }
        }

        public Box box {
            get { return _box; }
        }

        public bool IsBusy {
            get {
                return busy;
            }
            set {
                busy = value;
                onPropertyChanged(nameof(IsBusy));
                EditItemCommand.ChangeCanExecute();
            }
        }

        public Command EditItemCommand { get; }
        public Command BackCommand { get; }
        public Command ChangeBoxCommand { get; }

        internal void EditItem()
        {
            App.PushNavAsync(new CreateItem(_connectionService, _move, _box, _item));
        }

        public async void ChangeBox()
        {
            var answer = await 
                Application.Current.MainPage.DisplayActionSheet(
                    "Change Box", "Cancel", null, 
                    this._move.boxes.Select(box => box.name).ToArray());

            System.Diagnostics.Debug.WriteLine("=======> User selected new box of " + answer);
            Box newBox = this._move.boxes.SingleOrDefault(bx => bx.name == answer);

            String newId = await ItemDAO.MoveItem(_move.id, _item, _box, newBox);
            App.PopNavAsync();
        }

    }
}
