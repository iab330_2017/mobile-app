﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisualBoxManager.ViewModels.Home;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace VisualBoxManager.views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CreateItem : ContentPage
	{
        /// <summary>
        /// Construct a new CreateItem object in create new mode
        /// </summary>
        /// <param name="connectionService"></param>
        /// <param name="move"></param>
        /// <param name="box"></param>
		public CreateItem (IConnectionService connectionService, Move move, Box box)
		{
			InitializeComponent ();
            BindingContext = new CreateItemViewModel(connectionService, move, box);
		}

        /// <summary>
        /// Construct a new CreateItem object in edit mode
        /// </summary>
        /// <param name="connectionService"></param>
        /// <param name="move"></param>
        /// <param name="box"></param>
        /// <param name="item"></param>
        public CreateItem(IConnectionService connectionService, Move move, Box box, Item item)
        {
            InitializeComponent();
            BindingContext = new CreateItemViewModel(connectionService, move, box, item);
        }
    }
}