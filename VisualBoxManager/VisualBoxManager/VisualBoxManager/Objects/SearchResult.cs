﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VisualBoxManager.Objects
{
    class SearchResult
    {
        private Item _item;
        private Box  _box;

        public SearchResult(Item item, Box box)
        {
            _item = item;
            _box  = box;
        }

        public Item item
        {
            get { return _item; }
            set { _item = value; }
        }

        public Box box {
            get { return _box; }
            set { _box = value; }
        }

    }
}
