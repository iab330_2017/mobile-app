﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VisualBoxManager
{
    static class Config
    {
        public static readonly bool deployed = true;

        public static string getAddress()
        {
            if (deployed)
            {
                return  "pack-manager.herokuapp.com";
            }
            return "172.19.24.234"; //uni address
            //return "192.168.1.79";  //local dev address
        }


        public  static int port = 4567;
        public static Uri getUri() { return new Uri(getBaseUri());  }

        
        private static string getBaseUri() {
            if (deployed)
            {
                return "https://" + getAddress() + "/";

            }
            return "http://" + getAddress() + ":" + port.ToString() + "/";
        }

        internal static Uri getItemsForBoxURI(string boxID, string moveID)
        {
            return new Uri(getBoxesForMoveURI(moveID) + "/" + boxID + "/item/");
        }

        internal static Uri getLoginUri() { return new Uri(getBaseUri() + "login/"); }
        internal static Uri getLogoutUri() { return new Uri(getBaseUri() + "logout/"); }
        internal static Uri getUserUri() { return new Uri(getBaseUri() + "user/"); }

        internal static Uri getMovesUri() { return new Uri(getBaseUri() + "api/move/"); }

        internal static Uri getMoveDetailUri  (String moveID) { return new Uri(getBaseUri() + "api/move/" + moveID); }

        internal static Uri getBoxesForMoveURI(String moveID) { return new Uri(getMovesUri() + moveID + "/box"); }

        internal static Uri getRoomsForMoveURI(String moveId) { return new Uri(getMovesUri() + moveId + "/room/");}
    }
}