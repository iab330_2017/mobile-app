﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using VisualBoxManager.Objects;

namespace VisualBoxManager
{
    /// <summary>
    /// This class is to store information about one box
    /// </summary>
    public class Box
    {
        //private Room destinationRoom;
        private String _destinationRoomID;
        private ObservableCollection<Item> contents;
        private bool fragile;
        private bool packed;
        private BoxPriority priority;
        private bool sent;

        /// <summary>
        /// An empty constructor for creating an empty box
        /// </summary>
        public Box()
        {
            contents = new ObservableCollection<Item>();
        }

        /// <summary>
        /// Constructor for the class, gives the object a name, priority and a destination-roomId
        /// </summary>
        /// <param name="name"> Name of the box </param>
        /// <param name="priority"> THe priority of this box </param>
        /// <param name="roomId"> The roomId for the destination room of this box </param>
        public Box(string name, BoxPriority priority, String roomId)
        {
            this.name = name;
            this.priority = priority;
            this.contents = new ObservableCollection<Item>();
            _destinationRoomID = roomId;
        }

        /// <summary>
        /// An observable list of the items in this box
        /// </summary>
        public ObservableCollection<Item> Contents
        {
            get
            {
                return contents;
            }
        }

        public String destinationRoomId
        {
            get
            {
                return _destinationRoomID;
            }
            set
            {
                _destinationRoomID = value;
            }
        }
        public string id { get; set; }
        public string name { get; set; }

        public BoxPriority Priority
        {
            get
            {
                return priority;
            }
            set
            {
                priority = value;
            }
        }

        /// <summary>
        /// A method for deleting an item from <see cref="Contents"/>
        /// </summary>
        /// <param name="itemId"> The itemId of the Item that will be deleted from the list </param>
        public void DeleteItem(String itemId)
        {
            System.Diagnostics.Debug.WriteLine("=====> Deleting item from box.");
            for (int i = 0; i < this.contents.Count; i++)
            {
                if (this.contents[i].id == itemId)
                {
                    this.contents.RemoveAt(i);
                    break;
                }
            }
        }

        /// <summary>
        /// A method for clearing the <see cref="Contents"/> list
        /// </summary>
        public void NoItems()
        {
            contents.Clear();
        }

        /// <summary>
        /// Syncs a new list into the existing <see cref="Contents"/> list. Items in <see cref="items"/> that are not in contents wil be added to contents.
        /// </summary>
        /// <param name="items"> A list of all the items that should be in <see cref="Contents"/> </param>
        public void SyncItems(List<Item> items)
        {
            System.Diagnostics.Debug.WriteLine("=====> Syncing New items into box.");
            foreach (Item newItem in items)
            {
                bool found = false;
                for (int i = 0; i < this.contents.Count; i++)
                {
                    if (this.contents[i].id == newItem.id)
                    {
                        this.contents[i] = newItem;
                        found = true;
                    }
                }
                if (!found) this.contents.Add(newItem);
            }
        }


        public enum BoxPriority
        {
            Low = 0,
            Medium = 1,
            High = 2
        }
    }
}
