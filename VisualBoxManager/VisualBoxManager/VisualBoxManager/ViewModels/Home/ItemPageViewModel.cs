﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Input;
using iTextSharp.text;
using iTextSharp.text.pdf;
using VisualBoxManager;
using VisualBoxManager.ConnectionServices;
using VisualBoxManager.Interfaces;
using VisualBoxManager.ViewModels;
using VisualBoxManager.views;
using VisualBoxManager.views.home;
using Xamarin.Forms;

namespace VisualBoxManager.ViewModels.Home
{
    public class ItemPageViewModel : RootViewModel
    {
        private bool busy;
        private Item _selectedItem;
        private Move _move;
        private Box _currentBox;
        private bool refreshing = false;

        public ItemPageViewModel(IConnectionService connectionService, Move move) : base(connectionService)
        {
            _move = move;
            _currentBox = new Box();
            CreatNewItemCommand = new Command(() => OnCreateNew(), () => !IsBusy);
            PrintBoxLabelCommand = new Command(() => PrintBoxLabel(), () => !IsBusy);
            Refresh();
            IsBusy = false;
        }

        public ObservableCollection<Item> Items
        {
            get {
                return _currentBox.Contents;
            }
        }

        public Box CurrentBox {
            get { return _currentBox; }
            set {
                // Removes the change event listener for the old box. Then adds in the new one.
                _currentBox.Contents.CollectionChanged -= this.ItemsCollectionChanged;
                _currentBox = value;
                _currentBox.Contents.CollectionChanged += this.ItemsCollectionChanged;
                onPropertyChanged(nameof(CurrentBox));
                Refresh();
            }
        }

        public Item SelectedItem {
            get {
                return _selectedItem;
            }
            set {
                _selectedItem = value;
                if (value != null)
                {
                    IsBusy = false;
                    App.PushNavAsync(new ItemDetailPage(_move, _currentBox, _selectedItem));
                }
            }
        }

        internal async void DeleteItem(string id)
        {
            IsBusy = true;
            await ItemDAO.DeleteItem(_move.id, _currentBox, id);
            Refresh();
            IsBusy = false;
        }

        internal void EditItem(Item item)
        {
            App.PushNavAsync(new CreateItem(_connectionService, _move, _currentBox, item));
        }

        public Command CreatNewItemCommand { get; }
        private void OnCreateNew()
        {
            App.PushNavAsync(new CreateItem(_connectionService, _move, _currentBox));
        }
        public ICommand RefreshCommand {
            get {
                return new Command(Refresh);
            }
        }
        
        public async void Refresh()
        {
            IsBusy = true;
            if (refreshing) { return; }
            refreshing = true;
            if (_currentBox == null || string.IsNullOrEmpty(_currentBox.id) || string.IsNullOrEmpty(_move.id))
            {
                IsBusy = false;
                refreshing = false;
                return;
            }
            await ItemDAO.GetItemsForBox(_currentBox.id, _move.id);
            refreshing = false;
            IsBusy = false;
        }
        internal void ChangeBox(Item item)
        {
            //TODO: implement this. Maybe a popup to select the new box?
            //App.PushNavAsync(new CreateItem(_connectionService, _move, _currentBox, item));
        }

        public bool IsBusy
        {
            get
            {
                return busy;
            }
            set
            {
                busy = value;
                onPropertyChanged(nameof(IsBusy));
                CreatNewItemCommand.ChangeCanExecute();
                PrintBoxLabelCommand.ChangeCanExecute();
            }
        }

        private void ItemsCollectionChanged(object aSender, NotifyCollectionChangedEventArgs aArgs)
        {
            onPropertyChanged(nameof(Items));
        }

        public Command PrintBoxLabelCommand { get; }

        private async void PrintBoxLabel()
        {
            System.Diagnostics.Debug.WriteLine("====> About to generate PDF box label.");
            await RoomDAO.GetRoomsForMove(_move.id);

            try
            {
                DependencyService.Get<ISavePdf>().Save(_move, _currentBox);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("====> Unable to generate PDF. Error getting dependency. Exception: " + e.ToString());
            }
        }

    }
}
