﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace VisualBoxManager.ConnectionServices
{
    static class ItemDAO
    {
        private static User user = User.Instance();
        private static ConnectionService connection;

        static ItemDAO()
        {
            connection = ConnectionService.Instance();
        }

        /// <summary>
        /// Gets list of rooms for provided Move Id and syncs with move object in the model.
        /// </summary>
        /// <param name="moveID">ID of the move</param>
        /// <returns>Boolean success/fail</returns>
        public static async Task<bool> GetItemsForBox(String boxID, String moveID)
        {
            if(boxID == null || moveID == null)
            {
                System.Diagnostics.Debug.WriteLine("============>    Found a null parameter when getting items for box.");
                return false;
            }
            //Preload rooms as they will be needed to parse results.
            await RoomDAO.GetRoomsForMove(moveID);
            HttpRequestMessage message = new HttpRequestMessage(HttpMethod.Get, Config.getItemsForBoxURI(boxID, moveID));
            var response = await connection.MakeRequest(message);

            List<Item> responseResult = new List<Item>();
            var move = user.moves.SingleOrDefault(mv => mv.id == moveID);
            var box = move.boxes.SingleOrDefault(bx => bx.id == boxID);

            if (response.IsSuccessStatusCode)
            {
                //process result
                try
                {
                    String responseData = await response.Content.ReadAsStringAsync();
                    System.Diagnostics.Debug.WriteLine("=============>    Retrieved Items: " + responseData);
                    responseResult = JsonConvert.DeserializeObject<List<Item>>(responseData);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("=============>    Error deserializing object: " +
                                                       e.GetType().ToString() + "   Message: " + e.Message);
                }
            }
            else if(response.StatusCode == System.Net.HttpStatusCode.NotFound)
            {
                //No items yet in the box. All good just don't show anything.
                box.NoItems();
                return true;
            }
            else 
            {
                //TODO: opps not 2xx status code. What now?
                //Build custom exception handler
                //Redirect user to login if auth problem
                //Rollback and retry if 5xx problem
                //Alert user
            }

            if (0 < (responseResult.Count))
            {
                box.SyncItems(responseResult);
            }
            return true;
        }



        /// <summary>
        /// Sends a newly created item to the server to store in database
        /// </summary>
        /// <param name="boxID">ID of box to store item in</param>
        /// <param name="moveID">ID of move that contains the box.</param>
        /// <param name="item">The item to store</param>
        /// <returns>New ID of the item in the database</returns>
        public static async Task<string> CreateItemInBox(String boxID, String moveID, Item newItem)
        {
            HttpRequestMessage message = new HttpRequestMessage(HttpMethod.Post, Config.getItemsForBoxURI(boxID, moveID));
            message.Content = new StringContent(
                JsonConvert.SerializeObject(newItem, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore })
            );

            var response = await connection.MakeRequest(message);
            if (response.IsSuccessStatusCode)
            {
                String responseString = await response.Content.ReadAsStringAsync();
                System.Diagnostics.Debug.WriteLine("================>  Created item, response from server: " + responseString);
                try
                {
                    String itemId;
                    var responseResult = JsonConvert.DeserializeObject<Dictionary<String, String>>(responseString);
                    responseResult.TryGetValue("id", out itemId);
                    return itemId;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("=============>    Error deserializing object: " +
                                                       e.GetType().ToString() + "   Message: " + e.Message);
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Deletes an item from the database
        /// </summary>
        /// <param name="moveId">ID of move containing the item</param>
        /// <param name="box">the box object containing the item</param>
        /// <param name="itemId">Id of the item</param>
        /// <returns>Boolean success/failure</returns>
        public static async Task<bool> DeleteItem(String moveId, Box box, string itemId)
        {
            HttpRequestMessage message = new HttpRequestMessage(HttpMethod.Delete, Config.getItemsForBoxURI(box.id, moveId) + itemId + "/");
            var response = await connection.MakeRequest(message);
            if (response.IsSuccessStatusCode)
            {               
                box.DeleteItem(itemId);
            }
            return response.IsSuccessStatusCode;
        }

        /// <summary>
        /// Updates an existing item in the database
        /// </summary>
        /// <param name="moveId">ID of move containing the item</param>
        /// <param name="box">the box object containing the item</param>
        /// <param name="item">an instance of the item to update with changes made. ID value MUST match that of the item in the database</param>
        /// <returns>Boolean success/failure.</returns>
        public static async Task<bool> UpdateItem(String moveId, Box box, Item item)
        {
            HttpRequestMessage message = new HttpRequestMessage(HttpMethod.Put, Config.getItemsForBoxURI(box.id, moveId) + item.id + "/");
            message.Content = new StringContent(
                JsonConvert.SerializeObject(item, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore })
            );

            var response = await connection.MakeRequest(message);
            if (response != null && response.IsSuccessStatusCode)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// moves an item from it's current box to a different one.
        /// </summary>
        /// <param name="moveId">ID of move containing boxes</param>
        /// <param name="item">Instance of Item containing ALL details.</param>
        /// <param name="existingBox">box to remove item from</param>
        /// <param name="newBox">box to add item to</param>
        /// <returns></returns>
        public static async Task<String> MoveItem(String moveId, Item item, Box existingBox, Box newBox)
        {
            /*NOTE: I have some niggling concerns about data atomicity here. I don't think it will be a major problem
            because we are completly recreating the item and generating a unique id. If the item isn't deleted it won't
            cause any application problems and the user will still be able to delete the duplicate. If it isn't created
            in the new box but is deleted then the user will have to recreate the item and the data will be lost :-(
            The only scenario in which this might happen is if the network dropped out between the delete and write operataions.
            This is quite unlikely as the delete operation depeneds on a success of the write operation.*/

            System.Diagnostics.Debug.WriteLine("====> Attempting to move item to different box.");
            //Create item in new position.
            var newId = await CreateItemInBox(newBox.id, moveId, item);

            //Delete item from old position if create was successful.
            if (String.IsNullOrEmpty(newId))
            {
                return null;
            }

            bool deleteSuccess = await DeleteItem(moveId, existingBox, item.id);
            if (deleteSuccess)
            {
                return newId;
            }
            System.Diagnostics.Debug.WriteLine("====> WARNING: Creation of item in new location when moving item between boxes was succesful but deletion was not.");
            return newId;
        }
    }


}
