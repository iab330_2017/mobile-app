﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisualBoxManager.ViewModels.Home;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace VisualBoxManager.views.home
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SearchItemsPage : ContentPage
	{
		public SearchItemsPage (Move move, TabbedPage tabs)
		{
			InitializeComponent ();
            BindingContext = new SearchItemsViewModel(ConnectionService.Instance(), move, tabs);
        }
	}
}