﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisualBoxManager.ViewModels.Home;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace VisualBoxManager.views.home
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ItemDetailPage : ContentPage
	{
		public ItemDetailPage (Move move, Box box, Item item)
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel(ConnectionService.Instance(), move, box, item);
        }


	}
}