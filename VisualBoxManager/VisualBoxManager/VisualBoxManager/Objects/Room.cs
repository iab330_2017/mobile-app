﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VisualBoxManager
{
    /// <summary>
    /// A class for storing information about a room
    /// </summary>
    public class Room
    {
        private string _name;
        private string _id;

        /// <summary>
        /// Constructor for creating a room with a name and an ID
        /// </summary>
        /// <param name="name"> Name of the room </param>
        /// <param name="id"> The Id of the room </param>
        public Room(string name, string id)
        {
            _name = name;
            _id = id;
        }

        /// <summary>
        /// Constructor for creating a room with only a name
        /// </summary>
        /// <param name="name"> Name of the room</param>
        public Room(string name)
        {
            _name = name;
        }

        public Room(){}

        public string name {
            get { return _name; }
            set { _name = value; }
        }
        public string id{
            get { return _id; }
            set { _id = value; }
        }
    }
}
