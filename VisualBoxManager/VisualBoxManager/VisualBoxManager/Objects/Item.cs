﻿using System;
using System.Collections.Generic;
using System.Text;
using static VisualBoxManager.Box;

namespace VisualBoxManager
{
    /// <summary>
    /// A class for storing the information about an Item
    /// </summary>
    public class Item
    {
        private string _id;
        private string _name;
        private string _description;
        private bool _fragile;
        private bool _packed;
        private Room _destinationRoom;
        private BoxPriority _priority;

        /// <summary>
        /// Constructor for the Item object
        /// </summary>
        public Item()
        {
            name = "";
            id = "";
            description = "";
            fragile = false;
            packed = false;
            destRoom = new Room();
            priority = BoxPriority.Low;
        }

        public Item(string name) : this()
        {
            this.name = name;
        }

        public string id {
            get { return _id; }
            set { _id = value; }
        }
        public string name {
            get { return _name;  }
            set { _name = value; }
        }

        public string description {
            get { return _description;  }
            set { _description = value; }
        }
        
        public bool fragile {
            get { return _fragile;  }
            set { _fragile = value; }
        }

        public bool packed {
            get { return _packed;  }
            set { _packed = value; }
        }

        public Room destRoom {
            get { return _destinationRoom;  }
            set { _destinationRoom = value; }
        }

        /// <summary>
        /// The destination room property. It will point to an existing room, or create a new "Unknown" room, if it does not exist.
        /// </summary>
        public String destinationRoomId
        {
            get { return _destinationRoom.id; }
            set
            {
                User user = User.Instance();
                bool found = false;
                foreach (var move in user.moves)
                {
                    foreach (var room in move.rooms)
                    {
                        if (room.id == value)
                        {
                            _destinationRoom = room;
                            found = true;
                            break;
                        }
                    }
                }
                if (!found)
                {
                    _destinationRoom = new Room("Unknown", value);
                }
            }
        }
        public BoxPriority priority {
            get { return _priority; }
            set { _priority = value; }
        }


    }
}
