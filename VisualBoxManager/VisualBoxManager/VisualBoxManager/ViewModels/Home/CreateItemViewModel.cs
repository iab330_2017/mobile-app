﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisualBoxManager.ConnectionServices;
using VisualBoxManager.Objects.Validations;
using VisualBoxManager.views;
using Xamarin.Forms;
using static VisualBoxManager.Box;

namespace VisualBoxManager.ViewModels.Home
{
    public class CreateItemViewModel : RootViewModel
    {
        private Move _move;
        private Box _box;
        private bool _editing;
        private bool _busy;

        private Item _item;
        private string _errName;

        public CreateItemViewModel(IConnectionService connectionService, Move move, Box box) : base(connectionService)
        {
            _move = move;
            _box = box;
            _item = new Item();
            CancelCommand = new Command(() => OnCancel(), () => !(IsBusy));
            CreateNewItemCommand = new Command(async () => await OnSave(), () => !(IsBusy));
            CreateNewRoomCommand = new Command(() => CreateNewRoom());
            _move.rooms.CollectionChanged += new NotifyCollectionChangedEventHandler(RoomsCollectionChanged);
            Refresh();
            //SelectedRoom = 1;

        }

        public CreateItemViewModel(IConnectionService connectionService, Move move, Box box, Item item) : this(connectionService, move, box)
        {
            //This constructor is used when editing an item rather than simply creating a new one.
            _editing = true;
            _item = item;
            item.destRoom =  move.rooms.SingleOrDefault(rm => rm.id == item.destinationRoomId);

        }
        public Command CreateNewItemCommand { get; }
        public Command CancelCommand { get; }
        public Command CreateNewRoomCommand { get; }

        public Item item {
            get { return _item; }
        }

        public Box box {
            get { return _box; }
        }

        public string ErrItemName {
            get { return _errName;  }
            set {
                _errName = value;
                onPropertyChanged(nameof(ErrItemName));
            }
        }

        public ObservableCollection<Room> Rooms {
            get { return _move.rooms; }
        }

        public bool IsBusy {
            get {
                return _busy;
            }
            set {
                _busy = value;
                onPropertyChanged(nameof(IsBusy));
                onPropertyChanged(nameof(IsNotBusy));
                CreateNewItemCommand.ChangeCanExecute();
                CancelCommand.ChangeCanExecute();

            }
        }

        // Most minimalistic way to disable entrys while being busy
        public bool IsNotBusy => !_busy;

        private async void Refresh()
        {
            IsBusy = true;
            await RoomDAO.GetRoomsForMove(_move.id);
            onPropertyChanged(nameof(Rooms));
            IsBusy = false;
        }

        private void OnCancel()
        {
            IsBusy = false;
            App.PopNavAsync();
        }

        private async Task<bool> OnSave()
        {
            if (_editing)
            {
                return await UpdateItem();
            }
            IsBusy = true;
            bool success = false;

            ValidationResult result = Validator.ValidateName(item.name);
            ErrItemName = result.Message;

            if (!result.Error)
            {
                //Item newBox = new Box(_boxName, _boxPriority, SelectedRoom.id);
                item.id = await ItemDAO.CreateItemInBox(_box.id, _move.id, item);
                if (string.IsNullOrEmpty(item.id))
                {
                    ErrItemName = "Failed to add new box. Please try again";
                    IsBusy = false;
                }
                else
                {
                    ErrItemName = "Success!";
                    IsBusy = false;
                    App.PopNavAsync();
                    success = true;
                }
            }
            IsBusy = false;
            return success;
        }
        private async Task<bool> UpdateItem()
        {
            IsBusy = true;
            System.Diagnostics.Debug.WriteLine("==========>    Preparing to send box update to PAMS");
            //The below is not likely to be necessary if I bind directly to the items properties
            //if (item.destRoom != null)
            //{
            //    item.DestinationRoomID = SelectedRoom.id;
            //}
            //_editingBox.name = _boxName;
            //_editingBox.Priority = _boxPriority;

            //TODO: NOT YET IMPLEMENTED
            bool success = await ItemDAO.UpdateItem(_move.id, _box, _item);
            IsBusy = false;
            App.PopNavAsync();
            return success;
        }

        private void CreateNewRoom()
        {
            App.PushNavAsync(new CreateRoom(_connectionService, _move.id));
        }

        private void RoomsCollectionChanged(object aSender, NotifyCollectionChangedEventArgs aArgs)
        {
            onPropertyChanged(nameof(Rooms));
        }

    }
}
