﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace VisualBoxManager.views.home
{
    class SearchToolbarItem : ToolbarItem
    {
        private Move _move;
        private TabbedPage _tabbedPage;
        public SearchToolbarItem(Move move, TabbedPage tabs)
        {
            Text = "Search";
            //Icon = "VisualBoxManager.views.home.search.png";
            _move = move;
            _tabbedPage = tabs;
            Command = new Command(() => activated());
        }

        private void activated()
        {
            System.Diagnostics.Debug.WriteLine("====> Activated search fuction");
            App.PushNavAsync(new SearchItemsPage(_move, _tabbedPage));
        }
    }
}
