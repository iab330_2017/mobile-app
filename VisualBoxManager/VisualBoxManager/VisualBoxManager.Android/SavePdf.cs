﻿using System.IO;
using System.Linq;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Java.IO;
using Java.Lang;
using VisualBoxManager.Droid;
using VisualBoxManager.Interfaces;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency(typeof(SavePdf))]
namespace VisualBoxManager.Droid
{
    public class SavePdf : ISavePdf
    {
        private bool constuctorTrick;
        public SavePdf()
        {
            constuctorTrick = true;
        }
        public void Save(Move move, Box box)
        {

            string externalStorageState = global::Android.OS.Environment.ExternalStorageState;
            if (!Environment.MediaMounted.Equals(externalStorageState))
            {
                Toast.MakeText(Xamarin.Forms.Forms.Context, "Application requires access to Writeable SD card or internal public storage in order to print label.", ToastLength.Short).Show();
                return;
            }
                
            string application = "application/pdf";
            var externalPath = global::Android.OS.Environment.ExternalStorageDirectory.Path + "/box_labels/" + box.name +"_label.pdf";

            Java.IO.File folder = new Java.IO.File(global::Android.OS.Environment.ExternalStorageDirectory.Path + "/box_labels");
            if (!folder.Exists())
            {
                folder.Mkdir();
            }

            System.Diagnostics.Debug.WriteLine("====> Saving pdf to path: " + externalPath.ToString());

            System.IO.FileStream fs = new FileStream(Path.Combine(externalPath),
                FileMode.Create);
            Document document = new Document(PageSize.A5, 25, 25, 25, 25);
            PdfWriter writer = PdfWriter.GetInstance(document, fs);
            document.Open();
            document.AddTitle(box.name);
            try
            {
                document.Add(new Paragraph("DESTINATION ==>  " +
                                            move.rooms.SingleOrDefault(rm => rm.id == box.destinationRoomId)
                                                .name));
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("====> Unable to add destination room to label. Skipping.");
            }
            List itemList = new List(true);

            foreach (Item item in box.Contents)
            {
                itemList.Add(item.name);
            }

            document.Add(itemList);
            document.Close();
            writer.Close();
            fs.Close();

            Java.IO.File file = new Java.IO.File(externalPath);
            file.SetReadable(true);
            //Android.Net.Uri uri = Android.Net.Uri.Parse("file://" + filePath);
            Android.Net.Uri uri = Android.Net.Uri.FromFile(file);
            Intent intent = new Intent(Intent.ActionView);
            intent.SetDataAndType(uri, application);
            intent.SetFlags(ActivityFlags.ClearWhenTaskReset | ActivityFlags.NewTask);

            try
            {
                Xamarin.Forms.Forms.Context.StartActivity(intent);
            }
            catch (Exception)
            {
                Toast.MakeText(Xamarin.Forms.Forms.Context, "No Application Available to View PDF", ToastLength.Short).Show();
            }
        }
    }
}