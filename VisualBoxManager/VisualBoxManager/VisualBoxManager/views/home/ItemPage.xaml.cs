﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using VisualBoxManager.ViewModels.Home;

namespace VisualBoxManager
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ItemPage : ContentPage
    {
        private IConnectionService _connectionService;

        public ItemPage(IConnectionService connectionService, Move move)
        {
            InitializeComponent();
            _connectionService = connectionService;
            BindingContext = new ItemPageViewModel(connectionService, move);
        }
        protected override void OnAppearing()
        {
            ((ItemPageViewModel)BindingContext).Refresh();

        }

        protected override void OnDisappearing()
        {
            ((ItemPageViewModel)BindingContext).IsBusy = false ;

        }

        private void Delete_OnClicked(object sender, EventArgs e)
        {
            var x = ((MenuItem)sender).CommandParameter as Item;
            ((ItemPageViewModel)BindingContext).DeleteItem(x.id);
        }

        private void Edit_OnClicked(object sender, EventArgs e)
        {
            var x = ((MenuItem)sender).CommandParameter as Item;
            ((ItemPageViewModel)BindingContext).EditItem(x);
        }

        private void ChangeBox_OnClicked(object sender, EventArgs e)
        {
            var x = ((MenuItem)sender).CommandParameter as Item;
            ((ItemPageViewModel)BindingContext).ChangeBox(x);
        }

    }
}
