﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Text;

namespace VisualBoxManager
{
    /// <summary>
    /// A class for storing the neccesary information about a Move
    /// </summary>
    public class Move
    {
        public ObservableCollection<Box> boxes = new ObservableCollection<Box>();
        public ObservableCollection<Room> rooms = new ObservableCollection<Room>();

        /// <summary>
        /// Empty constructor
        /// </summary>
        public Move() { }

        /// <summary>
        /// A constructor for creating a Move with only a name
        /// </summary>
        /// <param name="name"></param>
        public Move(string name)
        {
            this.name = name;
        }

        /// <summary>
        /// A constructor for creating a Move with a name and an Id
        /// </summary>
        /// <param name="name"> Name of the move </param>
        /// <param name="id"> THe Id of the move, used to find the move in the cloud </param>
        public Move(string name, string id)
        {
            this.name = name;
            this.id = id;
        }

        public string name { get; set; }
        public string id { get; set; }

        /// <summary>
        /// Syncs the existing <see cref="boxes"/> with the new boxes, new boxes in the new list will be added to the old list.
        /// </summary>
        /// <param name="boxes"> A list of boxes that should be in <see cref="boxes"/> </param>
        public void SyncBoxes(List<Box> boxes)
        {
            foreach (Box newBox in boxes)
            {
                bool found = false;
                for (int i = 0; i < this.boxes.Count; i++)
                {
                    if (this.boxes[i].id == newBox.id)
                    {
                        this.boxes[i] = newBox;
                        found = true;
                    }
                }
                if (!found) this.boxes.Add(newBox);
            }
        }

        /// <summary>
        /// Syncs the existing <see cref="rooms"/> with the new rooms, new rooms in the new list will be added to the old list.
        /// </summary>
        /// <param name="rooms"> A list of boxes that should be in <see cref="rooms"/> </param>
        public void SyncRooms(List<Room> rooms)
        {
            foreach (Room newRoom in rooms)
            {
                bool found = false;
                for (int i = 0; i < this.rooms.Count; i++)
                {
                    if (this.rooms[i].id == newRoom.id)
                    {
                        this.rooms[i] = newRoom;
                        found = true;
                    }
                }
                if (!found) this.rooms.Add(newRoom);
            }
        }

        /// <summary>
        /// A method for deleting a box
        /// </summary>
        /// <param name="boxId"> The id of the box that will be deleted </param>
        public void DeleteBox(String boxId)
        {
            for(int i = 0; i < this.boxes.Count; i++)
            {
                if(this.boxes[i].id == boxId)
                {
                    this.boxes.RemoveAt(i);
                    break;
                }
            }
        }

    }
}
