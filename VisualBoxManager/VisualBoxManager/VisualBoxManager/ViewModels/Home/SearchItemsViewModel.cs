﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Text;
using VisualBoxManager.ConnectionServices;
using VisualBoxManager.Objects;
using Xamarin.Forms;

namespace VisualBoxManager.ViewModels.Home
{
    class SearchItemsViewModel : RootViewModel
    {
        private Move _move;
        private ObservableCollection<SearchResult> _results;
        private String _query;
        private bool busy;
        private SearchResult _selectedSearchResult;
        private TabbedPage _tabbedPage;
        
        public SearchItemsViewModel(IConnectionService connectionService, Move move, TabbedPage tabs) : base(connectionService)
        {
            _tabbedPage = tabs;
            _move = move;
            _results = new ObservableCollection<SearchResult>();
            SearchCommand = new Command(() => Search(), () => !IsBusy);
            _results.CollectionChanged += new NotifyCollectionChangedEventHandler(this.ResultsCollectionChanged);
        }
        public Command SearchCommand { get; }

        public ObservableCollection<SearchResult> Results {
            get { return _results; }
        }
        private void ResultsCollectionChanged(object aSender, NotifyCollectionChangedEventArgs aArgs)
        {
            onPropertyChanged(nameof(Results));
            onPropertyChanged(nameof(HasResults));
        }

        public String Query {
            get { return _query; }
            set { _query = value; }
        }

        public bool IsBusy {
            get {
                return busy;
            }
            set {
                busy = value;
                onPropertyChanged(nameof(IsBusy));
                SearchCommand.ChangeCanExecute();
            }
        }

        public bool HasResults {
            get {
                return (_results.Count == 0);
            }
        }

        public SearchResult SelectedItem {
            get {
                return _selectedSearchResult;
            }
            set {
                _selectedSearchResult = value;
                if (value != null)
                {
                    ((ItemPageViewModel)_tabbedPage.Children[(int)Tabs.items].BindingContext).CurrentBox = _selectedSearchResult.box;
                    _tabbedPage.CurrentPage = _tabbedPage.Children[(int)Tabs.items];
                    //App.PushNavAsync(_tabbedPage);
                    App.PopNavAsync();
                }
            }
        }

        /// <summary>
        /// Reload boxes from server.
        /// Reload all items from server
        /// Locates any item with name containing the query.
        /// Adds results to the collection so that they can be displayed to uesr.
        /// </summary>
        public async void Search()
        {
            System.Diagnostics.Debug.WriteLine("====> Searching for item with name: " + _query);
            IsBusy = true;
            await BoxDAO.GetBoxes(_move.id);
            foreach(Box box in _move.boxes)
            {
                await ItemDAO.GetItemsForBox(box.id, _move.id);
                foreach(Item item in box.Contents)
                {
                    if (item.name.Contains(_query))
                    {
                        _results.Add(new SearchResult(item, box));
                    }
                }
            }
            IsBusy = false;
        }

    }
}
